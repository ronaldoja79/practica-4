package com.example.hp.cuartapractica;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        Frag_Uno.OnFragmentInteractionListener, Frag_Dos.OnFragmentInteractionListener
{
    Button Frag_Uno, Frag_Dos;
    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Frag_Dos = findViewById(R.id.btnFragUno);
        Frag_Uno = findViewById(R.id.btnFragUno);
        Frag_Uno.setOnClickListener(this);
        Frag_Dos.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFragUno:
                Frag_Uno fragUno = new Frag_Uno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor, fragUno);
                transactionUno.commit();
                break;
            case R.id.btnFragDos:
                Frag_Dos fragDos = new Frag_Dos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor, fragDos);
                transactionDos.commit();
                break;
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
